'use strict';
module.exports = (sequelize, DataTypes) => {
  const ActivityType = sequelize.define('ActivityType', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    }
  }, {
    tableName: 'activity-type',
    timestamps: false
  });
  ActivityType.associate = function(models) {
    ActivityType.belongsTo(models.Activity, {foreignKey: 'id'})
  };
  return ActivityType;
};