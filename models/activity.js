'use strict';
module.exports = (sequelize, DataTypes) => {
  const Activity = sequelize.define('Activity', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    activityType: {
      type: DataTypes.INTEGER,
      references:{
          model: 'ActivityType',
          key: ' id'
      },
      allowNull: false,
      field: "activitytype"
    },
    userId: {
        type: DataTypes.INTEGER,
        references: {
            model: 'User',
            key: 'id'
        },
        allowNull: false,
        field: "userid"
    },
    duration: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    date: {
        type: DataTypes.DATE,
        allowNull: false
    },
    location: {
        type: DataTypes.STRING
    } 
  }, {
    tableName: 'activity',
    timestamps: false
  });
  Activity.associate = function(models) {
    Activity.hasMany(models.User, {as: 'user'})
    Activity.hasMany(models.ActivityType, {as: 'type'})
  };
  return Activity;
};