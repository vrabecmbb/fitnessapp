'use strict';
module.exports = (sequelize, DataTypes) => {
  const Token = sequelize.define('Token', {
    token: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    expired: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    tableName: 'tokens',
    timestamps: false
  });
  Token.associate = function(models) {
    // associations can be defined here
  };
  return Token;
};