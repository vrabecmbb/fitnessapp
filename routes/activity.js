const checkToken = require('../routes/middleware/check-token')

module.exports = (app,db) => {
    
    /**
     * GET /activity
     * param type - activity type
     * param page = number of page
     * param perPage = items per page
     */
    app.get("/activities", (req,res,next) => {

        const activityType = parseInt(req.query.type)
        const page = (req.query.page !== undefined && !isNaN(req.query.page)) ? req.query.page : 1
        const perPage = (req.query.perPage !== undefined && !isNaN(req.query.perPage)) ? req.query.perPage : undefined

        if (
            (isNaN(activityType) && req.query.type !== undefined) ||
            activityType < 0 ||
            (isNaN(page) && req.query.page !== undefined) ||
            page < 0 ||
            (isNaN(perPage) && req.query.perPage !== undefined) ||
            perPage < 0
            ){
                return res.status(400).json({
                    status: 'Error',
                    code: 400,
                    message: 'Bad request'
                })
        }
        console.log('activityType',activityType)
        

        let sql =  `SELECT * FROM activity`


        if (!isNaN(activityType)){
            sql += ` WHERE activityType = ${activityType}`
        }

        if (page & perPage){
            let offset = (page - 1) * perPage
            sql += ` LIMIT ${offset},${perPage}`
        }

        console.log(sql)

        db.sequelize.query(sql)
        .then(result => {
            res.status(200).json({
                status: "ok",
                code: 200,
                count: result.length,
                data: result[0]
            })
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({
                status: "Error",
                code: 500,
                error: err
            })
        })

    })

    /**
     * GET /activity/id
     * @param activityId - id of activity
     */
    app.get("/activity/:activityId", (req, res) => {
        const id = req.params.activityId
        
        db.Activity.findAll({
            where: {
                'id': id
            }
        })
        .then(activity => {
            if (activity){
                res.status(200).json({
                    status: 'ok',
                    code: 200,
                    data: activity
                })
            } else {
                res.status(404).json({
                    status: 'Error',
                    code: 404,
                    message: 'Activity not found'
                })
            }
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({
                status: 'Error',
                code: 500,
                error: err
            })
        })
    })

    app.post("/activities", checkToken, (req,res,next) => {
        const activity = req.body.activity
        const usertoken = req.headers.authorization.split(' ')[1]
        const decodedToken = jwt.verify(usertoken, app.config.jwt_key)
        const user = decodedToken.id
        const duration = req.body.duration
        const date = req.body.date,
        const location = req.body.location || ''

        date.Activity.create({
            activityType: activity,
            userId: user,
            duration: duration,
            date: date,
            location: location
        }).then(activity => {
            if (activity){
                res.status(200).json({
                    status: 'ok',
                    code: 200,
                    message: "Activity created successfully"
                })
            } else {
                return res.status(400).json({
                    status: 'error',
                    code: 400,
                    err: err
                })
            }
        }).catch(err => {
            res.status(500).json({
                status: 'Error',
                code: 500,
                error: err
            })
        })
    })

    /**
     * PATCH /activity/:activityId
     * parameters of model Activity
     */
    app.patch("/activity/:activityId", checkToken, (req, res, next) => {
        const id = req.params.activityId;
        const toUpdate = {}
        for (const param of req.body){
            toUpdate[param.propName] = param.value
        }
        db.Activity.update({id: id}, toUpdate)
        .then(result => {
            res.status(200).json({
                status: 'ok',
                code: 200,
                message: 'Activity updated'
            })
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({
                status: 'Error',
                code: 500,
                error: err
            })
        })
    })

    /**
     * DELETE /activity/:activityId
     */
    app.delete("/activity/:activityId", checkToken, (req, res, next) => {
        const id = req.params.activityId;
        db.Activity.remove({id: id})
        .then(result => {
            res.status(200).json({
                status: 'ok',
                code: 200,
                message: 'Activity deleted'
            })
        })
        .catch(err => {
            console.log(err)
            res.status(500).json({
                status: 'Error',
                code: 500,
                error: err
            })
        })
    })
}