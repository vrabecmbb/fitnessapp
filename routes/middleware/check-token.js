const jwt = require('jsonwebtoken')
const fs = require('fs')

const config = JSON.parse(fs.readFileSync(__dirname + '/../../config/config.json').toString());

module.exports = (req, res, next) => {

    try {
        const token = req.headers.authorization.split(" ")[1]
        const decoded = jwt.verify(token,config.jwt_key)
        req.userData = decoded
        next()
    } catch (error){
        return res.status(401).json({
            status: 'Error',
            code: 401,
            message: 'Authentication failed'
        })
    }
}