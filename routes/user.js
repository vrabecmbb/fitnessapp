const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const checkToken = require('../routes/middleware/check-token')
const moment = require('moment')

module.exports = (app, db) => {

    app.get("/test", (req, res) => {
        db.User.findAll({
            where: {
                email: 'jozo@fit.sk'
            }
        }).then((user) => {
            console.log(user[0].id)
        })
    });

    //Signup user
    app.post("/signup", (req, res) => {
        //is user exists?
        db.User.findAll({
            where: {
                email: req.body.email
            }
        }).then((result) => {
            console.log(result)

            if (result.length > 0) {
                res.status(409).json({
                    status: 'Error',
                    code: 409,
                    message: 'User exists'
                })
            } else {
                bcrypt.hash(req.body.password, 123, (err, hash) => {
                    if (err){
                        return res.status(500).json({
                            error: err
                        })
                    } else {
                        db.User.create({
                            email: req.body.email,
                            password: hash,
                            first_name: req.body.first_name,
                            last_name: req.body.last_name
                        }).then((user) => {
                            if (user){
                                return res.status(201).json({
                                    status: 'ok',
                                    code: 201,
                                    message: 'User created'
                                })
                            } else {
                                return res.status(400).json({
                                    error: err
                                })
                            }
                        }).catch((err) =>{
                            return res.status(500).json({
                                status: 'Error',
                                code: 500,
                                error: err
                            })
                        })
                    }
                })
            }
        })
    })

    /**
     * POST /signin
     * @param email
     * @param password
     */
    app.post("/signin", (req, res) => {
        db.User.findAll({
            where: {
                email: req.body.email
            }
        }).then((user) => {
            if (user.length < 1){
                return res.status(401).json({
                    status: 'Error',
                    code: 401,
                    message: 'User not found'
                })
            }
            bcrypt.compare(req.body.password, user[0].password, (err, result) => {
                if (err){
                    return res.status(401).json({
                        status: 'Error',
                        code: 401,
                        message: 'Authentication failed'
                    })
                }
                if (result){
                    const token = jwt.sign({
                        email: user[0].email,
                        id: user[0].id
                    },
                    app.config.jwt_key,
                    {
                        expiresIn: "1h"
                    }
                    );
                    db.Token.create({
                        token: token,
                        expired: moment().add(1,'hour').format('x')
                    })
                    return res.status(200).json({
                        status: 'ok',
                        code: 200,
                        message: 'Authentication successful',
                        token: token
                    })
                }
                res.status(401).json({
                    status: 'Error',
                    code: 401,
                    message: 'Authentication failed'
                })
            })
        })
    })

    //logout
    app.get("/logout", checkToken, (req, res, next) => {
        const token = req.headers.authorization.split(" ")[1]

        db.Token.findAll({
            where: {
                token: token
            }
        }).then((tokens) => {
            if (tokens.length < 1){
                res.status(401).json({
                    status: 'Error',
                    code: 401,
                    message: "Access forbiden"
                })
            } else {
                db.Token.destroy({
                    where: {
                        token: token
                    }
                }).then(() => {
                    res.status(200).json({
                        status: 'ok',
                        code: 200,
                        message: "Succesfully loged out"
                    })
                })
            }

        })
    })
};