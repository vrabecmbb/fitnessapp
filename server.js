//nodejs
var fs = require('fs')
var bodyParser = require('body-parser')

//express
var express = require('express')
var app = express()

//app
var db = require("./models");

//routes
var User = require("./routes/user");
var Activity = require("./routes/activity")

app.get("/", (req, res) => {
  res.status(200).json({
    status: 'ok'
  })
})

var config = JSON.parse(fs.readFileSync('config/config.json').toString());
app.config = config

app.use(bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 

User(app,db)
Activity(app,db)


app.listen(config.http_port, function(req, res){
    console.log(`App is listening on port ${config.http_port}`);
})

//testing
module.exports = app