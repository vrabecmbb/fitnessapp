process.env.NODE_ENV = 'test'

const chai = require('chai')
const expect = chai.expect
const request = require('supertest')

const app = request.agent("http://localhost:3000")

describe('homepage',() =>{
    it("will result 200", (done) => {
        app.get("/")
        .expect(200, done)
    })
})

describe('get activities',() =>{
    it("will result 200", (done) => {
        app.get("/activities")
        .expect(200, done)
    })
    
    it("will result 400 after bad request", (done) => {
        app.get("/activities?type=absdsd")
        .expect(400, done)
    })

    it("will result 400 after negative number of type", (done) => {
        app.get("/activities?type=-1")
        .expect(400, done)
    })
})

describe('authorisation fail', () => {
    it("will not sign", (done) => {
        app.post("/signin")
        .send({email: "test@test.sk",password: "23"})
        .expect(401, done)
    })
})
